﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;


namespace SQL_Connection_Reader
    {
    class Program
        {
        static void Main(string[] args)
            {
            //Create Connect Strem 
            string conStr = @"Data Source = .\SQLEXPRESS; Initial Catalog = ShopDB; Integrated Security = true;";
            
            //Create SQLConnection 
            SqlConnection connection = new SqlConnection(conStr);

            //Open SQLConnection
            connection.Open();

            //Create SQL Command 
            SqlCommand cmd = new SqlCommand("SELECT * FROM CUTUMERS" + connection);

            //2 method Create Command
            //SqlCommand cmd = connection.CreateCommand();
            //cmd.CommandText = "SELECT * FROM CUTUMERS";
            
            //Create SQL Reader 
            SqlDataReader reader = cmd.ExecuteReader();


            while (reader.Read())
                {
                for (int i = 0; i < reader.FieldCount; i++)
                    {
                    Console.WriteLine(reader.GetName(i) + ":" + reader[i]);
                    }
                Console.WriteLine(new string('-',20));
                }
            reader.Close();
            connection.Close();
            }
        }
    }
